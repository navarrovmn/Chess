package model;

import java.awt.Point;
import java.util.ArrayList;
import model.Piece.Team;

public class Queen extends Piece{
		public Queen(Team team){
			super(team);
		}
		
		public String imagePath(Team team){
			String path = "";
			if(team == Team.UP_TEAM){
				path = "icon/Brown Q_48x48.png";
			}else{
				path = "icon/White Q_48x48.png";
			}
			return path;
		}
		
		public ArrayList<ArrayList<Point>> move(Point point){
			ArrayList<ArrayList<Point>> movesQueen = (new Rook().move(point));
			movesQueen.addAll((new Bishop().move(point)));
			return movesQueen;
		}
	}

