package model;

import java.awt.Point;
import java.util.ArrayList;
import model.Piece.Team;

public class King extends Piece{
		public King(Team team){
			super(team);
		}
		
		public String imagePath(Team team){
			String path = "";
			if(team == Team.UP_TEAM){
				path = "icon/Brown K_48x48.png";
			}else{
				path = "icon/White K_48x48.png";
			}
			return path;
		}

		public ArrayList<ArrayList<Point>> move(Point point){
			int x = point.x;
			int y = point.y;
			ArrayList<ArrayList<Point>> movesKing = new ArrayList<>();
			
			ArrayList<Point> movesKing1 = new ArrayList<>();
			ArrayList<Point> movesKing2 = new ArrayList<>();
			ArrayList<Point> movesKing3 = new ArrayList<>();
			ArrayList<Point> movesKing4 = new ArrayList<>();
			ArrayList<Point> movesKing5 = new ArrayList<>();
			ArrayList<Point> movesKing6 = new ArrayList<>();
			ArrayList<Point> movesKing7 = new ArrayList<>();
			ArrayList<Point> movesKing8 = new ArrayList<>();
			
			movesKing1.add(new Point(x, y+1));
			movesKing2.add(new Point(x+1,y));
			movesKing3.add(new Point(x, y-1));
			movesKing4.add(new Point(x-1,y));
			movesKing5.add(new Point(x-1, y-1));
			movesKing6.add(new Point(x+1, y-1));
			movesKing7.add(new Point(x-1, y+1));
			movesKing8.add(new Point(x+1, y+1));
			
			movesKing.add(movesKing1);
			movesKing.add(movesKing2);
			movesKing.add(movesKing3);
			movesKing.add(movesKing4);
			movesKing.add(movesKing5);
			movesKing.add(movesKing6);
			movesKing.add(movesKing7);
			movesKing.add(movesKing8);
			
			
			return movesKing;
		}
			
	}
