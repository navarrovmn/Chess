package model;

import java.awt.Point;
import java.util.ArrayList;
import model.Piece.Team;

public class Pawn extends Piece {
	public Pawn(Team team){
		super(team);
		
	}
	
	public String imagePath(Team team){
		String path = "";
		if(team == Team.UP_TEAM){
			path = "icon/Brown P_48x48.png";
		}else{
			path = "icon/White P_48x48.png";
		}
		return path;
	}

	public ArrayList<ArrayList<Point>> move(Point point){
		ArrayList<ArrayList<Point>> movesPawn = new ArrayList<>();
		int x = point.x;
		int y = point.y;
		
		ArrayList<Point> movesPawn1 = new ArrayList<>();
		ArrayList<Point> movesPawn2 = new ArrayList<>();
		ArrayList<Point> movesPawn3 = new ArrayList<>();
		
		ArrayList<Point> movesPawn4 = new ArrayList<>();
		ArrayList<Point> movesPawn5 = new ArrayList<>();
		ArrayList<Point> movesPawn6 = new ArrayList<>();

		
		if (getTeam() == Team.UP_TEAM) {
			if(y == 1){
				movesPawn1.add(new Point(x,y+1));
				movesPawn1.add(new Point(x,y+2));
			}
			if(y<7 && y != 1){
				movesPawn1.add(new Point(x,y+1));
			}
			movesPawn2.add(new Point(x-1, y+1));
			movesPawn3.add(new Point(x+1, y+1));
		}else{
			if(y == 6){
				movesPawn4.add(new Point(x,y-1));
				movesPawn4.add(new Point(x,y-2));
			}
			if(y>0 && y!=6){
				movesPawn4.add(new Point(x,y-1));
				
			}
			movesPawn5.add(new Point(x-1, y-1));
			movesPawn6.add(new Point(x+1, y-1));
		}
		
		movesPawn.add(movesPawn1);
		movesPawn.add(movesPawn2);
		movesPawn.add(movesPawn3);
		movesPawn.add(movesPawn4);
		movesPawn.add(movesPawn5);
		movesPawn.add(movesPawn6);

		
		return movesPawn;
	}
	
    public boolean checkMoves(Point position, Point enemyPosition) {
               if (this.getTeam() == Team.UP_TEAM) {
                       if(position.y+1 == enemyPosition.y){
                               if(position.x == enemyPosition.x){
                                       return false;
                               }
                       }
               }else{
                       if(position.y-1 == enemyPosition.y){
                               if(position.x == enemyPosition.x){
                                       return false;
                               }
                       }
               }
               return true;
       }
       
       public Point justFront(Point position, Point enemyPosition) {
               Point result = null;
               if (this.getTeam() == Team.UP_TEAM) {
                       if(position.y+1 == enemyPosition.y || position.y+2 == enemyPosition.y){
                               if(position.x == enemyPosition.x){
                                       result = enemyPosition;
                               }
                       }
               }else{
                       if(position.y-1 == enemyPosition.y || position.y-2 == enemyPosition.y){
                               if(position.x == enemyPosition.x){
                                       result = enemyPosition;
                               }
                       }
                    }
               return result;
       }
}
