package model;

import java.awt.Point;
import java.util.ArrayList;
import model.Piece.Team;

public class Rook extends Piece {
	public Rook(Team team){
		super(team);
		
	}
	
	public Rook(){
		
	}

	public String imagePath(Team team){
		String path = "";
		if(team == Team.UP_TEAM){
			path = "icon/Brown R_48x48.png";
		}else{
			path = "icon/White R_48x48.png";
		}
		return path;
	}
	
	public ArrayList<ArrayList<Point>> move(Point point){
		int x = point.x;
		int y = point.y;
		ArrayList<ArrayList<Point>> movesRook = new ArrayList<>();
		ArrayList<Point> movesRookN = new ArrayList<>();
		ArrayList<Point> movesRookE = new ArrayList<>();
		ArrayList<Point> movesRookW = new ArrayList<>();
		ArrayList<Point> movesRookS = new ArrayList<>();
		
		int i, k, l;
		for(i=1; i<8; i++){
			k=x+i;
			if(k<8){
				movesRookE.add(new Point(k,y));
			}
			k=x-i;
			if(k>=0){
				movesRookW.add(new Point(k,y));
			}
			l=y-i;
			if(l>=0){
				movesRookN.add(new Point(x,l));
			}
			l=y+i;
			if(l<8){
				movesRookS.add(new Point(x,l));
			}
			
		}
		movesRook.add(movesRookE);
		movesRook.add(movesRookW);
		movesRook.add(movesRookN);
		movesRook.add(movesRookS);

		return movesRook;
	}
		
}