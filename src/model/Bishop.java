package model;
import java.awt.Point;
import java.util.ArrayList;
import model.Piece.Team;

public class Bishop extends Piece {

	public Bishop(Team team){
			super(team);
			
		}
		public Bishop() {
		}
		
		public String imagePath(Team team){
			String path = "";
			if(team == Team.UP_TEAM){
				path = "icon/Brown B_48x48.png";
			}else{
				path = "icon/White B_48x48.png";
			}
			return path;
		}
		
		public ArrayList<ArrayList<Point>> move(Point point){
			int x = point.x;
			int y = point.y;
			ArrayList<ArrayList<Point>> movesBishop = new ArrayList<>();
			ArrayList<Point> movesBishopNE = new ArrayList<>();
			ArrayList<Point> movesBishopNW = new ArrayList<>();
			ArrayList<Point> movesBishopSE = new ArrayList<>();
			ArrayList<Point> movesBishopSW = new ArrayList<>();
			
			int i, k, l;
			for(i=1; i<8; i++){
				k=x+i;
				l=y+i;
				if(k<8 && l<8){
					movesBishopSE.add(new Point(k,l));
				}
				k=x-i;
				l=y-i;
				if(k>=0 && l>=0){
					movesBishopNW.add(new Point(k,l));
				}
				k=x+i;
				l=y-i;
				if(k<8 && l>=0){
					movesBishopNE.add(new Point(k,l));
				}
				k=x-i;
				l=y+i;
				if(k>=0 && l<8){
					movesBishopSW.add(new Point(k,l));
				}
				
			}
			movesBishop.add(movesBishopSE);
			movesBishop.add(movesBishopNW);
			movesBishop.add(movesBishopNE);
			movesBishop.add(movesBishopSW);
			
			return movesBishop;
			
		}
			
	}
