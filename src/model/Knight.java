package model;

import java.awt.Point;
import java.util.ArrayList;
import model.Piece.Team;

public class Knight extends Piece{
	public Knight(Team team){
			super(team);
	}
	
	public String imagePath(Team team){
		String path = "";
		if(team == Team.UP_TEAM){
			path = "icon/Brown N_48x48.png";
		}else{
			path = "icon/White N_48x48.png";
		}
		return path;
	}

	public ArrayList<ArrayList<Point>> move(Point point){
		int x = point.x;
		int y = point.y;
		Point result = new Point(0,0);
		ArrayList<ArrayList<Point>> movesKnight = new ArrayList<>();
		int[] possible1 = {-1, 1};
		int[] possible2 = {-2, 2};
		
		for(int i = 0; i < possible1.length; i++){
			for(int j = 0; j < possible2.length; j++){
				result.x = x + possible1[i];
				result.y = y + possible2[j];
				if(result.x >= 0 && result.y >= 0 && result.x <= 7 && result.y <= 7){
					ArrayList<Point> movesKnight1 = new ArrayList<>();
					movesKnight1.add(new Point(result.x, result.y));
					movesKnight.add(movesKnight1);
				}
			}
		}
		
		for(int i = 0; i < possible2.length; i++){
			for(int j = 0; j < possible1.length; j++){
				result.x = x + possible2[i];
				result.y = y + possible1[j];
				if(result.x >= 0 && result.y >= 0 && result.x <= 7 && result.y <= 7){
					ArrayList<Point> movesKnight2 = new ArrayList<>();
					movesKnight2.add(new Point(result.x, result.y));
					movesKnight.add(movesKnight2);
				}
			}
		}
		return movesKnight;
	}
}
