package util;

import java.awt.Point;
import java.util.ArrayList;

import model.Piece;
import model.Square;

public class MoveHelper {

	public static ArrayList<Point> applyRestrictionInMoves(ArrayList<ArrayList<Point>> moves,
			ArrayList<Square> squareList,Point piecePosition,  Piece piece) {
		
		ArrayList<Point> result = new ArrayList<>();
		for(int i = 0; i < moves.size(); i++){
			ArrayList<Point> temp =  moves.get(i);
			outerloop:
			for(int j = 0; j < temp.size(); j++){
				for(int k = 0; k < squareList.size(); k++){
					if(temp.get(j).x == squareList.get(k).getPosition().x && temp.get(j).y == squareList.get(k).getPosition().y){
						if(squareList.get(k).getPiece() != null){
							if(!squareList.get(k).getPiece().pieceIsOnMyTeam(piece)){
								if(piece.checkMoves(piecePosition, squareList.get(k).getPosition())){
									result.add(squareList.get(k).getPosition());
								}
							}
							break outerloop;
						}else{
							if(piece.checkMoves(piecePosition,squareList.get(k).getPosition())){
								Point auxiliar;
								auxiliar = piece.justFront(piecePosition, squareList.get(k).getPosition());
								result.add(auxiliar);
							}else{
								result.add(squareList.get(k).getPosition());
							}
						}
					}
				}
			}
		}
		return result;
	}
}
