package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Square;
import control.SquareControl;
import model.Piece;
import model.Pawn;
import model.Rook;
import model.Knight;
import model.King;
import model.Queen;
import model.Bishop;
import model.Piece.Team;


public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color possibleMoves = Color.YELLOW;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, possibleMoves);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;
			square.getPosition().setLocation(gridBag.gridx, gridBag.gridy);

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {

		Piece pawn = new Pawn(Team.UP_TEAM);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(pawn);
		}

		Piece rook = new Rook(Team.UP_TEAM);
		this.squareControl.getSquare(0, 0).setPiece(rook);
		this.squareControl.getSquare(0, 7).setPiece(rook);
		
		Piece knight = new Knight(Team.UP_TEAM);
		this.squareControl.getSquare(0, 1).setPiece(knight);
		this.squareControl.getSquare(0, 6).setPiece(knight);

		Piece bishop = new Bishop(Team.UP_TEAM);
		this.squareControl.getSquare(0, 2).setPiece(bishop);
		this.squareControl.getSquare(0, 5).setPiece(bishop);

		Piece queen = new Queen(Team.UP_TEAM);
		this.squareControl.getSquare(0, 4).setPiece(queen);

		Piece king = new King(Team.UP_TEAM);
		this.squareControl.getSquare(0, 3).setPiece(king);

		Piece wpawn = new Pawn(Team.DOWN_TEAM);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(wpawn);
		}
		
		Piece wrook = new Rook(Team.DOWN_TEAM);
		this.squareControl.getSquare(7, 0).setPiece(wrook);
		this.squareControl.getSquare(7, 7).setPiece(wrook);

		Piece wknight = new Knight(Team.DOWN_TEAM);
		this.squareControl.getSquare(7, 1).setPiece(wknight);
		this.squareControl.getSquare(7, 6).setPiece(wknight);

		Piece wbishop = new Bishop(Team.DOWN_TEAM);
		this.squareControl.getSquare(7, 2).setPiece(wbishop);
		this.squareControl.getSquare(7, 5).setPiece(wbishop);

		Piece wqueen = new Queen(Team.DOWN_TEAM);
		this.squareControl.getSquare(7, 4).setPiece(wqueen);

		Piece wking = new King(Team.DOWN_TEAM);
		this.squareControl.getSquare(7, 3).setPiece(wking);
	}
}
