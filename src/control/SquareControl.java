package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import util.MoveHelper;
import model.Piece;
import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_POSSIBLE_MOVES = Color.YELLOW;

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color possibleMoves;
	private Color saveColorBeforeHover;

	private Square selectedSquare;
	private Square newPosition;
	private ArrayList<Square> squareList;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED, DEFAULT_COLOR_POSSIBLE_MOVES);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color possibleMoves) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.possibleMoves = possibleMoves;

		this.squareList = new ArrayList<>();
		createSquares();
	}
	
	public void setColorOnPossibleMoves(ArrayList<Point> points, Color color){
		Point point;
		for(int i=0; i < 64; i++){
			point = squareList.get(i).getPosition();
			for(int j = 0; j < points.size(); j++){
				if(points.contains(point)){
					squareList.get(i).setColor(color);
				}
			}
			
		}
	}

	public void resetColorOnPossibleMoves(ArrayList<Point> points){
		Point point;
		for(int i=0; i < 64; i++){
			point = squareList.get(i).getPosition();
			for(int j = 0; j < points.size(); j++){
				if(points.contains(point)){
					resetColor(squareList.get(i));
				}
			}
		}
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		this.saveColorBeforeHover = square.getColor();
		square.setColor(this.colorHover);
	}
	
	
	@Override
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) {
			Piece squareTemp = this.selectedSquare.getPiece();
			Point savePoint = this.selectedSquare.getPosition();
			if (!this.selectedSquare.equals(square)) {
				this.newPosition = square;
				if(squareTemp != null){
					moveContentOfSelectedSquare(square);
					ArrayList<ArrayList<Point>> listPosition =  squareTemp.move(savePoint);
					for(int i = 0; i < listPosition.size(); i++){
						resetColorOnPossibleMoves(listPosition.get(i));
					}
				}
			} else {
				ArrayList<ArrayList<Point>> listPosition =  square.getPiece().move(savePoint);
				ArrayList<Point> filterList = MoveHelper.applyRestrictionInMoves(listPosition, this.squareList,this.selectedSquare.getPosition(), this.selectedSquare.getPiece());
				resetColorOnPossibleMoves(filterList);
				unselectSquare(square);
			}
			savePoint = square.getPosition();
			this.saveColorBeforeHover = this.getGridColor(savePoint.x, savePoint.y);
		} else {
			selectSquare(square);	
			if(square.getPiece() != null){
				ArrayList<ArrayList<Point>> listPosition =  square.getPiece().move(square.getPosition());
				ArrayList<Point> filterList = MoveHelper.applyRestrictionInMoves(listPosition, this.squareList,this.selectedSquare.getPosition(), this.selectedSquare.getPiece());
				setColorOnPossibleMoves(filterList,this.possibleMoves);
			}
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if (this.selectedSquare != square) {
			square.setColor(this.saveColorBeforeHover);
		} else {
			square.setColor(this.colorSelected);
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square){
		ArrayList<ArrayList<Point>> listPosition = this.selectedSquare.getPiece().move(this.selectedSquare.getPosition());
		ArrayList<Point> filterList = MoveHelper.applyRestrictionInMoves(listPosition, this.squareList, this.selectedSquare.getPosition(), this.selectedSquare.getPiece());
		if(filterList.contains(this.newPosition.getPosition())){
			square.setPiece(this.selectedSquare.getPiece());
			this.selectedSquare.removeImage();
			unselectSquare(square);
		}else{
			throw new RuntimeException("Movimento Inválido");
		}
		
	}

	private void selectSquare(Square square) {
		if (square.haveImagePath()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
}
